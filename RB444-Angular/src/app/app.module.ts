import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule  }   from '@angular/forms';
import { InterceptorService } from './http.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { AsideComponent } from './components/aside/aside.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { SliderComponent } from './components/slider/slider.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AllsportHighlightComponent } from './components/allsport-highlight/allsport-highlight.component';
import { InplayComponent } from './components/inplay/inplay.component';
import { TableRowComponent } from './components/table-row/table-row.component';
import { FullmarketComponent } from './components/fullmarket/fullmarket.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { ThousandSuffixesPipe } from './helpers/thousand-suffixes.pipe';
import { MyprofileComponent } from './components/myprofile/myprofile.component';
import { RollingCommissionComponent } from './components/rolling-commission/rolling-commission.component';
import { AccountStatementComponent } from './components/account-statement/account-statement.component';
import { BetHistoryComponent } from './components/bet-history/bet-history.component';
import { ProfitLossComponent } from './components/profit-loss/profit-loss.component';
import { ActivityLogComponent } from './components/activity-log/activity-log.component';
import { AuthService } from './services/auth.service';
import { HttpService } from './services/http.service';
import { SessionService } from './services/session.service';
import { ToastrModule } from 'ngx-toastr';
import { StackSettingsComponent } from './components/stack-settings/stack-settings.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelPropagation: true
};

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SidebarComponent,
    AsideComponent,
    SliderComponent,
    AllsportHighlightComponent,
    InplayComponent,
    TableRowComponent,
    FullmarketComponent,
    ThousandSuffixesPipe,
    MyprofileComponent,
    RollingCommissionComponent,
    AccountStatementComponent,
    BetHistoryComponent,
    ProfitLossComponent,
    ActivityLogComponent,
    StackSettingsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    CarouselModule,
    NgbModule,
    PerfectScrollbarModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [AuthService, HttpService, SessionService, InterceptorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
