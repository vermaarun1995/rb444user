import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-stack-settings',
  templateUrl: './stack-settings.component.html',
  styleUrls: ['./stack-settings.component.css']
})
export class StackSettingsComponent implements OnInit {

  isCollapsed: boolean = true;

  constructor(private service : HttpService) { }

  GetStakeLimit() {
    this.service.getData('http://api.rb444.in/api/Setting/GetStakeLimit')
      .pipe(map(response => {
        return response;
      }),
        catchError(() => {
          return of([]);
        }))
      .subscribe(response => {
        console.log(response);
      });
  }

  ngOnInit(): void {
  }

}
