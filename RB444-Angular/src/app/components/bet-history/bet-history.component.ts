import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-bet-history',
  templateUrl: './bet-history.component.html',
  styleUrls: ['./bet-history.component.css']
})
export class BetHistoryComponent implements OnInit {

  todayDate = new Date();
  maxDate = {year: this.todayDate.getFullYear(), month: this.todayDate.getMonth(), day: this.todayDate.getDate()};

  betList : any[] = [];
  betHistoryForm : FormGroup;

  constructor(private service : HttpService, private fb : FormBuilder, private notification : NotificationService) { 
    this.betHistoryForm = this.fb.group({
      startData : [this.maxDate],
      starttime : [''],
      endDate : [this.maxDate],
      endTime : [''],
      sportId : ['', [Validators.required]],
      isSettlement : ['']
    });
  }

  pData = {
    "userId": 13,
    "sportId": 4,
    "isSettlement": 2,
    "startDate": null,
    "endDate": null,
    "startTime": null,
    "endTime": null,
    "todayHistory": true,
    "columnName": null,
    "orderByColumn": 0,
    "PageNumber":1,
    "PageSize":10
  };

  getBetHistory() {

    if(this.betHistoryForm.controls.sportId.errors !== null){
      this.notification.showError("Please select sport.");
      return;
    }

    this.service.postData('http://api.rb444.in/api/BetApi/GetBetHistory', this.pData)
      .pipe(map(response => {
        return response;
      }),
        catchError(() => {
          return of([]);
        }))
      .subscribe(response => {
        if(response.isSuccess == true && response.data !== null){
          this.betList = response.data.betList;
        }
      });
  }

  ngOnInit(): void {
  }

}
