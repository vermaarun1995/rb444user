import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { interval, of, Subscription } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-fullmarket',
  templateUrl: './fullmarket.component.html',
  styleUrls: ['./fullmarket.component.css']
})
export class FullmarketComponent implements OnInit, OnDestroy {

  constructor(private service : HttpService, private activatedRoute : ActivatedRoute, private authService : AuthService) {
    this.authService._isLoginUser.subscribe(
      (res) => this.isUserLogin=res)
   }

  matchOddsData : any = [];

  subscription: Subscription = new Subscription();
  isUserLogin : boolean = false;

  getMarketsOfEventList:any = (eventId: number, sportId: number) =>{
    this.service.post('exchange/market/getMarketsOfEventList', { "eventId": eventId, "sportId": sportId})
    .pipe(map(response => {
      return response;
    }),
    catchError(() => {
      return of([]);
    }))
    .subscribe(response => {
      if(response.data != null){
        for (let [key, value] of Object.entries(response.data)) {
          if(key == "matchOddsData"){
            this.matchOddsData = value;
          }
        }
      }
    });
  }

  exEventId:number = 0;
  exMarketId:number = 0;
  selectionId:number = 0;
  selectedIndex:number = 0;
  oddsSlip:string = "slipBack";
  selectedValue:number =  0;

  openOrderRow:any = (index:number, exEventId:number, exMarketId:number, selectionId:number, slip:string, price:number) => {
    this.selectedIndex = index;
    this.exEventId = exEventId;
    this.exMarketId = exMarketId;
    this.selectionId = selectionId;
    this.oddsSlip = slip;
    this.bidPriceInput = price;
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(paramsId => {
      this.getMarketsOfEventList(paramsId.eventId, paramsId.sportId);
      const subscriptionInterval = interval(3000);
      if(this.isUserLogin){
        this.subscription = subscriptionInterval.pipe().subscribe(x => {
          this.getMarketsOfEventList(paramsId.eventId, paramsId.sportId);
        });      
      }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /******** Match Odds Bid Price *******/

  bidPriceInput : number = 0;
  bidOddPrice : number = 0;
  oddBook : boolean = false;
  oddBookPrice : number = 0;

  setBidPrice($event:any){
    if(typeof $event == 'object'){
      this.bidOddPrice = $event.target.value;
    }else{
      this.bidOddPrice = $event;
    }    
    this.oddBookPrice = Math.ceil(this.bidPriceInput * this.bidOddPrice)-this.bidOddPrice;
    this.oddBook = true;
  }

  numberPlus(input:number){
    console.log(input)
  }
  numberMinus(input:number){    
    console.log(input)
  }



}
