import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.css']
})
export class ActivityLogComponent implements OnInit {

  constructor(private service: HttpService) { }

  activityLog : any[] = []

  getActiveLog() {
    this.service.getData('http://api.rb444.in/api/Common/GetUserActivityLog')
      .pipe(map(response => {
        return response;
      }),
        catchError(() => {
          return of([]);
        }))
      .subscribe(response => {
        if(response.isSuccess == true && response.data !== null){
          this.activityLog = response.data;
        }
      });
  }

  ngOnInit(): void {
    this.getActiveLog();
  }

}
