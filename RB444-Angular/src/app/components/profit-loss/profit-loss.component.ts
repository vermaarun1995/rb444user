import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profit-loss',
  templateUrl: './profit-loss.component.html',
  styleUrls: ['./profit-loss.component.css']
})
export class ProfitLossComponent implements OnInit {

  todayDate = new Date();
  maxDate = {year: this.todayDate.getFullYear(), month: this.todayDate.getMonth(), day: this.todayDate.getDate()};

  constructor() { }

  ngOnInit(): void {
  }

}
