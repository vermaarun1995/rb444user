import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpService } from 'src/app/services/http.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.css']
})
export class MyprofileComponent implements OnInit {
  
  changePasswordForm : FormGroup;
  isFormSubmit = false;
  constructor(private service : HttpService, private modalService: NgbModal, private fb : FormBuilder) { 
    this.changePasswordForm = this.fb.group(
      {
        oldPassword : ['', [Validators.required]],
        newPassword : ['', [Validators.required]],
        confirmPass : ['', [Validators.required]]
      }
    );
  }

  closeResult = '';

  openRollingCommissionModal(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  openChangePasswordModal(content:any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  formSubmit(){
    this.isFormSubmit = true;
    console.log(this.changePasswordForm);
  }

  ngOnInit(): void {
  }

}
